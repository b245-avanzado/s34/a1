const express = require("express");

const port = 8080;

const app = express();

app.use(express.json());

// mockdata
let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

// GET "/home" Start
app.get("/home", (request, response) => {
	response.send("This is the homepage!");
});
// GET "/home" End

// GET "/items" Start
app.get("/items", (request, response) => {
	response.send(items);
})
// GET "/items" End

// DELETE "/delete-item" Start
		app.delete("/delete-item/:index", (request, response) => {
			let indexToBeDeleted = parseInt(request.params.index);

			if(indexToBeDeleted < items.length) {
				
				let deletedItem = items.splice(indexToBeDeleted, 1);

				response.send(deletedItem);

			} else {
				response.status(404).send("Page not found. 404!");
			}
			
		})
// DELETE "/delete-item" End




app.listen(port, () => console.log(`Server is running at port ${port}`))